package com.mds.jms.Listener;

import static org.junit.Assert.*;

import javax.jms.TextMessage;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.mds.jms.listener.ConsumerListener;

public class ConsumerListenerTest {
	
	private TextMessage textMessage;

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testOnMessage() {
		ConsumerListener listener=new ConsumerListener();
		listener.onMessage(textMessage);
		//fail("Not yet implemented");
		assertNull(textMessage);
	}

}
